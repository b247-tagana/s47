// console.log("Hello, B247!");


// document refers to the whole page.
console.log(document);

// querySelector() is a method that can be used to select a specific element from our document.
console.log(document.querySelector("#txt-first-name"));

/*
	Alternative methods we can use aside from querySelector() in retrieving elements.

	document.getElementById("txt-first-name");
	document.getElementsByClassName();
	document.getElementsByTagName();


*/


// Example:
// document.getElementById() - access an element similar to querySelector()
console.log(document.getElementById("txt-first-name"));

// document.getElementsByClassName() - method returns a collection of elements with a specified className/s.
// It returns an HTMLCollection and the property is read-only.
console.log(document.getElementsByClassName('txt-inputs'));

// document.getElementsByTagName() - method returns a collection of elements with a specified tagName.
// It returns an HTMLCollection and the property is read-only.
console.log(document.getElementsByTagName('input'));


const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(txtLastName)
console.log(spanFullName);


/*
	Event: click, hover, keypress, keyup and others.

	Event Listeners:
		Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function/task.


	Syntax:
		selectedElement.addEventListener('event', function);


*/


// ".innerHTML" - is a property of an element which considers all the children of the selected element as a string.
// ".value" - the value contained in the input field.
// txtFirstName.addEventListener('keyup', (event) => {

// 	spanFullName.innerHTML = txtFirstName.value

// });

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
});

txtLastName.addEventListener('keyup', (event) => {
	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
});

/*
	The "event" argument/parameter contains the information of the triggered event.
	
	The "event.target" contains the element where the event happened.

	The "event.target.value" gets the value of the input object(similar to the txtFirstName.value).
	

*/


// Another way to write the code for event listener:
txtFirstName.addEventListener('keyup', printFirstName);

function printFirstName(event) {
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}



const labelFirstName = document.querySelector("#label-first-name");
console.log(labelFirstName);

labelFirstName.addEventListener('click', (e) => {

	console.log(e)
	alert("You clicked the first name label.")

})

txtLastName.addEventListener('keyup', printLastName);

function printLastName(event) {

	 spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};


const labelLastName = document.querySelector("#label-last-name");
console.log(labelLastName);

labelLastName.addEventListener('click', (e) => {

	console.log(e)
	alert("You clicked the last name label.")

})

